package com.yang.spinach.common.utils;

import com.yang.spinach.frame.utils.spring.SpringUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Copyright: Copyright (c) 2016,${year},
 * @Description: ${todo}
 */

public class ScheduledJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledJob.class);

    public ScheduledJob() {
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        //doSomething
        String className = dataMap.getString("className");
        String methodName = dataMap.getString("methodName");
        System.out.println("执行任务的class:" + className + " ,方法为:" + methodName + "  ...................................");
        try {
            Object clazz = SpringUtils.getBean(Class.forName(className));
            Method method = clazz.getClass().getDeclaredMethod(methodName, new Class[]{});
            method.setAccessible(true);
            method.invoke(clazz);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
