package com.yang.spinach.bussiness.system.dao;

import com.yang.spinach.pagePlugin.Pagination;
import com.yang.spinach.bussiness.system.entity.Dict;
import com.yang.spinach.frame.dao.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * @author <Auto generate>
 * @version 2015-05-31 15:46:09
 * @see com.yang.spinach.bussiness.system.entity.dao.Dict
 */
public interface DictDao extends MyMapper<Dict> {

	List<Dict> selectByColumn(String column);

	List<Dict> listPage(@Param("pagination") Pagination pagination,
			@Param("dict") Dict dict);

}
